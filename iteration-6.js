// Iteración #6: Función swap

// Crea una función llamada `swap()` que reciba un array y dos parametros que sean indices del array. La función deberá intercambiar la posición de los valores de los indices que hayamos enviado como parametro. Retorna el array resultante.
// Sugerencia de array:

const arr = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño'];
function swap(param, x,y){
    let temp = param[x];
    param[x] = param[y];
    param[y] = temp;
    
    console.log(param);
}

swap(arr, 2, 3);