// Iteración #1: Mix for e includes
// Dado el siguiente javascript usa forof para recorrer el array de películas, genera un nuevo array con las categorías de las películas e imprime por consola el array de categorías. Ten en cuenta que las categorías no deberían repetirse. Para filtrar las categorías puedes ayudarte de la función .includes()

const movies = [{
        title: 'Madagascar',
        duration: 192,
        categories: ['comedia', 'aventura']
    },
    {
        title: 'Spiderpan',
        duration: 122,
        categories: ['aventura', 'acción']
    },
    {
        title: 'Solo en Whatsapp',
        duration: 223,
        categories: ['comedia', 'thriller']
    },
    {
        title: 'El gato con guantes',
        duration: 111,
        categories: ['comedia', 'aventura', 'animación']
    },
]

let list = [];

for (let movie of movies) {
    for (let values of movie.categories) {
        if (!list.includes(values)) {
            list.push(values);
        }
    }
}

console.log(list);