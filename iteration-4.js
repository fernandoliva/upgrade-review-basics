// Iteración #4: Métodos findArrayIndex
// Crea una función llamada `findArrayIndex` que reciba como parametros un array de textos y un texto y devuelve la posición del array cuando el valor del array sea igual al valor del texto que enviaste como parametro. Haz varios ejemplos y compruebalos.
// Sugerencia de función:


// Ej array:
const arr = ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote']

function findArrayIndex(array, text) {
    let position = 1;
    for (key of arr) {
        if (key === text) {
            console.log(`El texto es ${key} y su posición es la ${position}`);
            return;
        }
        position++;
    }


}

findArrayIndex(arr, 'Mosquito');